/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :    Chiffrement de messages                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Colome-charlotte                                             *
*                                                                             *
*  Nom-prénom2 : Pomies-guillaume                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   projet.c                                                *
*                                                                             *
******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <wchar.h>
#include <wctype.h>
#include <locale.h>
#include <projet.h>

// Liste des fonctions du programme
int verifAlphaNum(wchar_t *message);
int verifAlpha(wchar_t *message);
void convertirAccents(wchar_t *message);
void chiffrageCesar(wchar_t *message, int decalage);
void dechiffrageCesar(wchar_t *message, int decalage);
void chiffrageVigenere(wchar_t *message, wchar_t *cle);
void dechiffrageVigenere(wchar_t *message, wchar_t *cle);
void affichage(wchar_t *message, wchar_t mode);
void clearBuffer();

// Fonction de vérification alphanumérique
// Retourne 0 s'il y a un caractère spéciale
// Si tous les caractères sont bons, cela retourne 1
int verifAlphaNum(wchar_t *message){

	wchar_t charactereSpeciaux[50]=
    	{'>','<','#','(','{','[',']','}',')','/','*','-','+','.',
    	'@','|','&','~','"','_','`','^','?',';',',',':','!','%',
    	'$',L'§',L'µ',L'£',L'¤','*',L'€','=',L'°',L'²','\'','\\',
    	'0','1','2','3','4','5','6','7','8','9'};

	for(unsigned i = 0; i < wcslen(message); i++){
    	for(unsigned j = 0; j < 50; j++){
        	if(message[i] == charactereSpeciaux[j]){
            	return 0;
        	}
    	}
	}
	return 1;
}

// Fonction de vérification alphabétique
// Retourne 0 s'il y a un caractère non alphabétique
// Si tous les caractères sont bons, cela retourne 1
int verifAlpha(wchar_t *message){
	wchar_t numbers[10] = L"0123456789";

	for(unsigned i = 0; i < wcslen(message); i++){
    	for(unsigned j = 0; j < 10; j++){
        	if(message[i] == numbers[j]){
            	return 0;
        	}
    	}
	}
	return 1;
}

// Fonction de remplacements des accents
// Demande un tableau wchar_t
void convertirAccents(wchar_t *message){
	wchar_t accents[54] = L"ÀÁ ÃÄÅàáâãäåÒÓÔÕÖØòóôõöøÈÉÊËèéêëÌÍÎÏìíîïÙÚÛÜùúûüÿÑñÇç";
	wchar_t sansAcc[54] = L"AAAAAAaaaaaaOOOOOOooooooEEEEeeeeIIIIiiiiUUUUuuuuyNnCc";

	for(unsigned i = 0; i < wcslen(message); i++){
    	for(unsigned j = 0; j < 54; j++){
        	if(message[i] == accents[j]){
            	message[i] = sansAcc[j];
        	}
    	}
	}
}

// Chiffrage Cesar
// Demande un tableau wchar_t
// Demande un entier
void chiffrageCesar(wchar_t *message, int decalage){
	for(unsigned i = 0; i < wcslen(message); i++){
    	if(message[i] >= 'a' && message[i] <= 'z'){
        	message[i] = 'a' + ((message[i] - 'a') + decalage) % 26;
    	}
    	if(message[i] >= 'A' && message[i] <= 'Z'){
        	message[i] = 'A' + ((message[i] - 'A') + decalage) % 26;
    	}
    	if(message[i] >= '0' && message[i] <= '9'){
        	message[i] = '0' + ((message[i] - '0') + decalage) % 10;
    	}
	}
}

// Déchiffrage Cesar
// Demande un tableau wchar_t
// Demande un entier
void dechiffrageCesar(wchar_t *message, int decalage){
	for(unsigned i = 0; i < wcslen(message); i++){
    	if(message[i] >= 'a' && message[i] <= 'z'){
        	message[i] = 'a' + ((message[i] - 'a') + 26 - decalage) % 26;
    	}
    	if(message[i] >= 'A' && message[i] <= 'Z'){
        	message[i] = 'A' + ((message[i] - 'A') + 26 - decalage) % 26;
    	}
    	if(message[i] >= '0' && message[i] <= '9'){
        	message[i] = '0' + ((message[i] - '0') + 10 - decalage) % 10;
    	}
	}
}

// Chiffrage Vigenère
// Demande 2 tableaux wchar_t
void chiffrageVigenere(wchar_t *message, wchar_t *cle){
	wchar_t alphaMaj[26] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for(unsigned i = 0; i < wcslen(message); i++){
    	int cleL;
    	int nbEspaces;
    	wchar_t letterCode = cle[(i - nbEspaces )% wcslen(cle)];

    	for(unsigned j = 0; j < 26; j++){
        	if(letterCode == alphaMaj[j]){
            	cleL = j;
        	}
    	}

    	if(message[i] >= 'a' && message[i] <= 'z'){
        	message[i] = 'a' + ((message[i] - 'a') + cleL) % 26;
    	}
    	if(message[i] >= 'A' && message[i] <= 'Z'){
        	message[i] = 'A' + ((message[i] - 'A') + cleL) % 26;
    	}
    	if(message[i] >= '0' && message[i] <= '9'){
        	message[i] = '0' + ((message[i] - '0') + cleL) % 10;
    	}
    	if(message[i] == 32){
        	nbEspaces++;
    	}
	}
}

// Déchiffrage Vigenère
// Demande 2 tableaux wchar_t
void dechiffrageVigenere(wchar_t *message, wchar_t *cle){
	wchar_t alphaMaj[26] = L"ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	for(unsigned i = 0; i < wcslen(message); i++){
    	int cleL;
    	int nbEspaces;
    	wchar_t letterCode = cle[(i - nbEspaces ) % wcslen(cle)];

    	for(unsigned j = 0; j < 26; j++){
        	if(letterCode == alphaMaj[j]){
            	cleL = j;
        	}
    	}
    	if(message[i] >= 'a' && message[i] <= 'z'){
        	message[i] = 'a' + ((message[i] - 'a') + 26 - cleL) % 26;
    	}
    	if(message[i] >= 'A' && message[i] <= 'Z'){
        	message[i] = 'A' + ((message[i] - 'A') + 26 - cleL) % 26;
    	}
    	if(message[i] >= '0' && message[i] <= '9'){
        	message[i] = '0' + ((message[i] - '0') + 10 - cleL) % 10;
    	}
    	if(message[i] == 32){
        	nbEspaces++;
    	}
	}
}

// Fonction pour sauvegarder le résultat
// Demande le message en tableau wchar_t
void affichage(wchar_t *message, wchar_t mode){
	FILE* fichier = NULL;
	fichier = fopen("traduction.txt", "w");

	if(fichier != NULL){
		if (mode=='c'){
			fwprintf(fichier, L"Chiffrage du message : %ls", message);
		}else{
			fwprintf(fichier, L"Dechiffrage du message : %ls", message);
		}
    	fclose(fichier);
	}
}

// Fonction pour vider le buffer
void clearBuffer(){
	wchar_t c;
	c = getwchar();
	while(L'\n' != c){
    	c = getwchar();
	}
}

// Fonction principale
int main()
{
	// Variable Locale
	struct lconv *loc;
	setlocale(LC_ALL, "");
	loc=localeconv();

	// création tableau de caractères
	wchar_t message[200];

	// Entrée du message par l'utilisateur
	printf("Veuillez entrer une chaine de caractère valide (200 max): ");
	fgetws(message, 200, stdin);

	// Si message de l'utilisateur incorrect
	while(verifAlphaNum(message)== 0){
    	printf("Message incorrect, veuillez recommencer !\n");
    	printf("Veuillez entrer une chaine de caractère valide : ");
    	fgetws(message, 200, stdin);
	}

	// Choix de l'algorithme de l'utilisateur
	printf("Veuillez choisir l'algorithme voulu :\n");
	printf("- César ( c )\n");
	printf("- Vigenere ( v )\n");
	printf("Veuillez saisir l’algorithme choisi : ");

	wchar_t algo = fgetwc(stdin);

	// Si le choix de l'algorithme est incorrect
	while(algo != 'c' && algo != 'v'){
    	printf("Algorithme incorrect, veuillez recommencer !\n");
    	printf("Veuillez saisir l’algorithme choisi : ");
    	clearBuffer();
    	algo = fgetwc(stdin);
	}

	// Choix du mode de l'utilisateur
	printf("Veuillez choisir le mode voulu :\n");
	printf("- Chiffrage ( c )\n");
	printf("- Déchiffrage ( d )\n");
	printf("Veuillez saisir le mode choisi : ");

	clearBuffer();
	wchar_t mode = fgetwc(stdin);

	// Si le choix du mode est incorrect
	while(mode != 'c' && mode != 'd'){
    	printf("Mode incorrect, veuillez recommencer !\n");
    	printf("Veuillez saisir le mode choisi : ");
    	clearBuffer();
    	mode = fgetwc(stdin);
	}

	// Définition des codes pour les différents algorithmes
	if(algo == 'c'){

    	printf("Veuillez choisir votre decalage (chiffre) : ");
    	wchar_t decalage[10];
    	wchar_t *endString;
    	clearBuffer();
    	fgetws(decalage, 10, stdin);

    	while(wcstol(decalage, &endString, 10) == 0){
        	printf("Decalage incorrect, veuillez recommencer !\n");
        	printf("Veuillez choisir votre decalage (chiffre) : ");
        	clearBuffer();
        	fgetws(decalage, 10, stdin);
    	}

    	if(mode == 'c'){
        	chiffrageCesar(message, wcstol(decalage, &endString, 10));
    	} else if(mode == 'd'){
        	dechiffrageCesar(message, wcstol(decalage, &endString, 10) );
    	}

    	affichage(message, mode);
    	printf("Votre message codé a été enregistré dans un fichier texte !\n");


	} else if(algo == 'v'){

    	printf("Veuillez choisir votre clé (10 lettres max) : ");
    	wchar_t cle[10];
    	clearBuffer();
    	fgetws(cle, 10, stdin);


    	// Si message de l'utilisateur incorrect
    	while(verifAlpha(cle)== 0){
        	printf("Message incorrect, veuillez recommencer !\n");
        	printf("Veuillez entrer une chaine de caractère valide : ");
        	fgetws(cle, 10, stdin);
    	}

    	convertirAccents(cle);

    	// Enleve le \n
    	cle[wcslen(cle) - 1] = '\0';

    	for(unsigned i = 0; i < wcslen(cle); i++) {
        	cle[i] = towupper(cle[i]);
    	}

    	if(mode == 'c'){
        	chiffrageVigenere(message, cle);
    	} else if(mode == 'd'){
        	dechiffrageVigenere(message, cle);
    	}

    	affichage(message, mode);
    	printf("Votre message codé a été enregistré dans un fichier texte !\n");

	}

	return 0;
}
