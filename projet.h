/******************************************************************************
*  ASR => M2101                                                               *
*******************************************************************************
*                                                                             *
*  N° de Sujet : 3                                                            *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Intitulé :    Chiffrement de messages                                      *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom-prénom1 : Colome-charlotte                                             *
*                                                                             *
*  Nom-prénom2 : Pomies-guillaume                                             *
*                                                                             *
*******************************************************************************
*                                                                             *
*  Nom du fichier :   projet.h                                                *
*                                                                             *
******************************************************************************/

int verifAlphaNum(wchar_t *message);
int verifAlpha(wchar_t *message);
void convertirAccents(wchar_t *message);
void chiffrageCesar(wchar_t *message, int decalage);
void dechiffrageCesar(wchar_t *message, int decalage);
void chiffrageVigenere(wchar_t *message, wchar_t *cle);
void dechiffrageVigenere(wchar_t *message, wchar_t *cle);
void affichage(wchar_t *message, wchar_t mode);
void clearBuffer();
